////  SwiftUI2_BookingTicketsApp.swift
//  SwiftUI2_BookingTickets
//
//  Created on 18/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_BookingTicketsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
