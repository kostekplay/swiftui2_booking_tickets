////  BookingView.swift
//  SwiftUI2_BookingTickets
//
//  Created on 18/02/2021.
//  
//

import SwiftUI

struct BookingView: View {
    
    @State var bookedSeats: [Int] = [1,10,25,30,45,59,60]
    @State var selectedSeats : [Int] = []
    
    @State var date: Date = Date()
    
    @State var selectedTime = "11:30"
    
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false, content: {
            
            HStack{
                Button(action: {presentation.wrappedValue.dismiss()}, label: {
                    Image(systemName: "chevron.left")
                        .font(.title2)
                        .foregroundColor(.white)
                })
                Spacer()
            }
            .overlay(
                Text("Select Seats")
                    .font(.title2)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
            )
            .padding()
            
            GeometryReader{reader in
                
                let width = reader.frame(in: .global).width
                Path{ path in
                    path.move(to: CGPoint(x: 0, y: 50))
                    path.addCurve(to: CGPoint(x: width, y: 50), control1: CGPoint(x: width / 2, y: 0), control2: CGPoint(x: width / 2, y: 0))
                }
                .stroke(Color.gray,lineWidth: 1.5)
            }
            .frame(height: 50)
            .padding(.top,20)
            .padding(.horizontal,35)
            
            let totalSeats: Int = 60 + 4
            
            let leftSide = 0..<totalSeats/2
            let rightSide = totalSeats/2..<totalSeats
            
            HStack(spacing: 30, content: {
                
                let columns = Array(repeating: GridItem(.flexible(),spacing: 10), count: 4)
                
                LazyVGrid(columns: columns, spacing: 13, content: {
                   
                    ForEach(leftSide, id: \.self) { index in
                        
                        let seat = index >= 29 ? index - 1: index
                        
                        SeatView(index: index, seat: seat, selectedSeats: $selectedSeats, bookedSeats: $bookedSeats)
                    }
                })
                
                LazyVGrid(columns: columns, spacing: 13, content: {

                    ForEach(rightSide,id: \.self){ index in
                        
                        let seat = index >= 35 ? index - 2 : index - 1
                        
                        SeatView(index: index, seat: seat, selectedSeats: $selectedSeats, bookedSeats: $bookedSeats)
                    }
                })
            })
            .padding(.horizontal)
            .padding(.top, 30)
            
            Text(selectedSeats.description)

            HStack(spacing: 15, content: {
                
                RoundedRectangle(cornerRadius: 4)
                    .stroke(Color.gray)
                    .frame(width: 20, height: 20)
                    .overlay(
                        Image(systemName: "xmark")
                            .font(.caption)
                            .foregroundColor(.gray)
                    )
                
                Text("Booked")
                    .font(.caption)
                    .foregroundColor(.white)
                
                RoundedRectangle(cornerRadius: 4)
                    .stroke(Color.blue,lineWidth: 2)
                    .frame(width: 20, height: 20)
                
                Text("Available")
                    .font(.caption)
                    .foregroundColor(.white)
                
                RoundedRectangle(cornerRadius: 4)
                    .fill(Color.blue)
                    .frame(width: 20, height: 20)
                Text("Selected")
                    .font(.caption)
                    .foregroundColor(.white)
                
            })
            .padding(.top, 25)
            
            HStack {
                
                Text("Date:")
                    .font(.title3)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                
                Spacer()
                
                DatePicker("", selection: $date, displayedComponents: .date)
                    .labelsHidden()
            
            }
            .padding()
            
            ScrollView(.horizontal, showsIndicators: false, content: {
                
                HStack(spacing: 15, content: {
                    
                    ForEach(time, id: \.self) { timing in
                        
                        Text(timing)
                            .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                            .foregroundColor(.white)
                            .padding(.vertical)
                            .padding(.horizontal,30)
                            .background(Color.blue)
                            .opacity(selectedTime == timing ? 1 : 0.2)
                            .cornerRadius(10)
                            .onTapGesture {
                                selectedTime = timing
                            }
                    }
                    
                })
                .padding(.horizontal)
                
            })
            
            HStack(spacing: 15, content: {
                
                VStack(alignment: .leading, spacing: 16, content: {
                    Text("\(selectedSeats.count) seats")
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .font(.callout)
                    Text("$ \(selectedSeats.count * 12)")
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .font(.title)
                        .foregroundColor(.yellow)
                
                })
                .padding(.horizontal, 24)
                
                Spacer()
                
                Button(action: {}, label: {
                    /*@START_MENU_TOKEN@*/Text("Button")/*@END_MENU_TOKEN@*/
                        .foregroundColor(.white)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.blue)
                        .cornerRadius(16)
                })
                .padding(.horizontal, 24)
                
            })
            .padding(.vertical, 24)
            .padding(.top, 16)
        
        })
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct BookingView_Previews: PreviewProvider {
    static var previews: some View {
        BookingView()
            .previewDevice("iPhone 11 Pro")
            .preferredColorScheme(.dark)
    }
}
