////  SeatView.swift
//  SwiftUI2_BookingTickets
//
//  Created on 19/02/2021.
//  
//

import SwiftUI

struct SeatView: View {
    
    var index: Int
    var seat: Int
    
    @Binding var selectedSeats: [Int]
    @Binding var bookedSeats: [Int]
    
    var body: some View {
        
        ZStack{
            
            RoundedRectangle(cornerRadius: 8)
                .stroke(bookedSeats.contains(seat) ? Color.gray : Color.blue, lineWidth: 2)
                .frame(height: 30)
                .background(
                    selectedSeats.contains(seat) ? Color.blue : Color.clear
                )
                .opacity(index == 0 || index == 28 || index == 35 || index == 63 ? 0 : 1)
            
            if bookedSeats.contains(seat){
                Image(systemName: "xmark")
                    .foregroundColor(.gray)
            }
            
        }
        .contentShape(Rectangle())
        .onTapGesture {
            
            if selectedSeats.contains(seat){
                selectedSeats.removeAll { (removeSeat) -> Bool in
                    return removeSeat == seat
                }
                return
            }
            selectedSeats.append(seat)
        }
        .disabled(bookedSeats.contains(seat) || (index == 0 || index == 28 || index == 35 || index == 63))
    }
}

struct SeatView_Previews: PreviewProvider {
    static var previews: some View {
        SeatView(index: 1, seat: 10, selectedSeats: .constant([]) , bookedSeats: .constant([]))
    }
}
