////  TestView.swift
//  SwiftUI2_BookingTickets
//
//  Created on 18/02/2021.
//  
//

import SwiftUI

//struct TestView: View {
//
//    @State var location = CGPoint.zero
//
//    var body: some View {
//        VStack {
//            Color.red.frame(width: 100, height: 100)
//                .overlay(circle)
//            Text("Location: \(Int(location.x)), \(Int(location.y))")
//        }
//        .coordinateSpace(name: "stack")
//    }
//
//    var circle: some View {
//        Circle()
//            .frame(width: 25, height: 25)
//            .gesture(drag)
//            .padding(5)
//    }
//
//    var drag: some Gesture {
//        DragGesture(coordinateSpace: .named("stack"))
//            .onChanged { info in location = info.location }
//    }
//}




//struct TestView: View {
//    var body: some View {
//        OuterView()
//            .background(Color.red)
//            .coordinateSpace(name: "Custom")
//    }
//}
//
//struct OuterView: View {
//    var body: some View {
//        VStack(alignment: .center) {
//            Text("Top")
//            InnerView()
//                .background(Color.green)
//            Text("Bottom")
//        }
//    }
//}
//
//struct InnerView: View {
//    var body: some View {
//        HStack(alignment: .center)  {
//            Text("Left")
//            GeometryReader { geo in
//                Text("Center")
//                    .background(Color.blue)
//                    .onTapGesture {
//                        print("Global center: \(geo.frame(in: .global).midX) x \(geo.frame(in: .global).midY)")
//                        print("Custom center: \(geo.frame(in: .named("Custom")).midX) x \(geo.frame(in: .named("Custom")).midY)")
//                        print("Local center: \(geo.frame(in: .local).midX) x \(geo.frame(in: .local).midY)")
//                    }
//            }
//            .background(Color.orange)
//            Text("Right")
//        }
//    }
//}







struct TestView: View {
    
    @State private var showingDetail = false
    
    var body: some View {
        Button("Show Detail") {
            showingDetail = true
        }
        .sheet(isPresented: $showingDetail, content: {
            DetailView()
        })
    }
}

struct DetailView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        Button("Dismiss Me") {
            presentationMode.wrappedValue.dismiss()
        }
    }
}


struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        TestView()
    }
}
