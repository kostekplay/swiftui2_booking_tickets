////  ContentView.swift
//  SwiftUI2_BookingTickets
//
//  Created on 18/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TestView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
